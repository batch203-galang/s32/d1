let http = require("http");

const port = 4000;

// Mock Database
let directory = [
	{
		name: "Brandon",
		email: "brandon@mail.com"
	},
	{
		name: "Jobert",
		email: "jobert@mail.com"
	}
]

const server = http.createServer((request, response) => {
	// When the '/users' route is accessed with a method of "GET" we will send the directory (mock database) list of users



	if(request.url == "/users" && request.method == "GET"){
		response.writeHead(200, {"Content-Type":"application/json"});

		response.write(JSON.stringify(directory));

		response.end();
	}

	// We want to receive the content of the request and save it to the mock database.
		// Content will be retrieve from the request body.
		// Parse the received JSON request body to JavaScript Object
		// add the parse object to the directory (mock database)
	if(request.url == "/users" && request.method == "POST"){

		// This will act as a placeholder for the resource/data to be created later on.
		let requestBody = "";

		// 1st step
		// data step - this reads "data" stream and process it as a request body.
		request.on("data", (data) =>{
			// data received
			console.log(data);

			// Assigns the data retrieved from the data to stream to requestBody
			// at this point, "requestBody" has the 
			requestBody += data;

			// to show that the chunk of information is stored in the requestBody.
			console.log(requestBody);
		});
			request.on("end", () =>{
			console.log(typeof requestBody);
			
			requestBody = JSON.parse(requestBody);
			console.log(typeof requestBody);

			directory.push(requestBody);
			console.log(directory);

			response.writeHead(200, {"Content-Type":"application/json"});
			response.write(JSON.stringify(requestBody));
			response.end();
		});
	}

	// =============== FOR PRACTICE ===============
	// Update
	// Create a "/users" route that will be accessed with "PUT" method and will update the name values to full name. Use the email address as search key to update the name property.
		// Example: before: name: "Brandon", after: "name": "Brandon Clarkson"
		
	// Delete
	// Create "/users" route that will be accessed with "DELETE" method and will remove a user in our mock database. Use the email address as search key for removing a specific user.

	// Hint: You may use array methods for this routes.

});

server.listen(port);

console.log(`Server running at localhost:${port}`);

